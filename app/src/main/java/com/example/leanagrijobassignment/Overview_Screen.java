package com.example.leanagrijobassignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.leanagrijobassignment.model.Movie;

public class Overview_Screen extends AppCompatActivity {

    String overview;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview__screen);
        textView = findViewById(R.id.textView);
        textView.setText(getIntent().getStringExtra("Overview"));
    }
}
