package com.example.leanagrijobassignment;

import com.example.leanagrijobassignment.adater.CustomListAdapter;
import com.example.leanagrijobassignment.app.AppController;
import com.example.leanagrijobassignment.model.Movie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

public class MainActivity extends Activity {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();

    // Movies json url
    private static final String url = "https://api.themoviedb.org/3/discover/movie?api_key=4de664f989b4f585c74ad67dc4662cd2";
    private ProgressDialog pDialog;
    private List<Movie> movieList = new ArrayList<Movie>();
    private ListView listView;
    private CustomListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list);
        adapter = new CustomListAdapter(this, movieList);
        listView.setAdapter(adapter);

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        // changing action bar color
        getActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#1b1b1b")));

        // Creating volley request obj
        JsonArrayRequest movieReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();

                        // Parsing json
                        JSONObject obj = new JSONObject();
                        JSONArray ary = new JSONArray();

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                obj = response.getJSONObject(i);
                                ary = obj.getJSONArray("results");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            for (int j = 0; j < ary.length(); j++) {
                                Movie movie = new Movie();

                                try {
                                    movie.setTitle(ary.getJSONObject(j).getString("title"));
                                    movie.setThumbnailUrl(ary.getJSONObject(j).getString("poster_path"));
                                    movie.setRating(((Number) ary.getJSONObject(j).get("vote_average"))
                                            .doubleValue());
                                    movie.setYear(ary.getJSONObject(j).getInt("release_date"));
                                    movie.setOverview(ary.getJSONObject(j).getString("overview"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                // adding movie to movies array
                                movieList.add(movie);

                            }

                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(movieReq);


        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                Intent intent = new Intent(MainActivity.this, Overview_Screen.class);
                //Get the value of the item you clicked
                String overview = movieList.get(position).getOverview();
                intent.putExtra("Overview", overview);
                startActivity(intent);

            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case R.id.sortasc:
                ArrayList<String> movieNames = new ArrayList<>();
                for (int i = 0; i < movieList.size(); i++) {
                    movieNames.add(movieList.get(i).getTitle());
                }
                Collections.sort(movieNames);
                for (int i = 0; i < movieList.size(); i++) {
                    System.out.println(movieList.get(i));
                }

                adapter = new CustomListAdapter(this, movieList);
                listView.setAdapter(adapter);

                adapter.notifyDataSetChanged();
                return true;
            case R.id.sortdesc:

                ArrayList<String> movieNames2 = new ArrayList<>();
                for (int i = 0; i < movieList.size(); i++) {
                    movieNames2.add(movieList.get(i).getTitle());
                }
                Collections.reverse(movieNames2);
                for (int i = 0; i < movieList.size(); i++) {
                    System.out.println(movieList.get(i));
                }

                adapter = new CustomListAdapter(this, movieList);
                listView.setAdapter(adapter);

                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}